#!/bin/sh

FLAG=$1
echo Adding new-flag-$FLAG

cp new-flag-1.yml new-flag-$FLAG.yml
sed -i '' "s/new-flag-1/new-flag-$FLAG/g" new-flag-$FLAG.yml
git add -A
git commit -m "add new-flag-$FLAG"
git push origin